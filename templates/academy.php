<h1>Academy</h1>
<p>Blijf op de hoogte van de snelle ontwikkelingen in computerland. <br>
	Ontdek onze Tips & Tricks over branding, het maken van een website, groeimarketing en nog veel meer.</p>
<div class="academy-container">
	<?php
	$wpUrl = "https://eskidoos.be/wp-json/wp/v2/posts";
	$rawFile = file_get_contents($wpUrl);

	if ($rawFile) {

		$json = json_decode($rawFile);

		foreach ($json as $post) {
			$title = $post->title->rendered;
			$slug = $post->slug;
			$mediaId = $post->featured_media;

			if (is_int($mediaId)) {
				$imgUrl = "https://eskidoos.be/wp-json/wp/v2/media/" . $mediaId;
				$rawFile = file_get_contents($imgUrl);

				if ($rawFile) {
					$json = json_decode($rawFile);
					$image = $json->media_details->sizes->medium->source_url;
					$altText = $json->alt_text;
				}
			}

			echo '<article>
			<a href="https://eskidoos.be/academy/' . $slug . '?utm_source=plugin" target="_blank" rel="noreferrer noopener">
				<div class="academy-item__imageholder"><img src="' . $image . '" alt="' . $altText . '" /></div>
				<span>' . $title . '</span>
			</a>
		</article>';
		}
	}
	?>
</div>