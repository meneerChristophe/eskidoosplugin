<div class="wrap">
  <h1>ESKIDOOS support</h1>
  <p>
     Heb je hulp nodig?<br>
     Contacteer ons gerust<br>
     Wij zijn elke werkdag open van 10u tot 19u.<br>
  </p>
  <div class="contacthow">
    <div class="contactblock">
      <h2>Mail ons</h2>
      <p>Je krijgt een antwoord binnen de 24u op een werkdag.</p>
      <a href="mailto:christophe@eskidoos.be" class="supporteskidooslink" target="_blank">Support mailen</a>
    </div>
    <div class="contactblock">
      <h2>Bel ons</h2>
      <p>Wij staan voor je klaar! (of bellen snel terug als we in meeting zijn)</p>
      <a href="tel:0032487499997" class="supporteskidooslink">+32 487 49 99 97</a>
    </div>
    <div class="contactblock">
      <h2>Chat met ons</h2>
      <p>Rechtsonderaan, zichtbaar tijdens de kantooruren.</p>
      <a href="https://eskidoos.be" target="_blank" class="supporteskidooslink">ESKIDOOS website</a>
    </div>
  </div>

</div>
