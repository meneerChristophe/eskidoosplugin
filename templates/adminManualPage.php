<h1>Handleiding</h1>
<p>Ontdek onze handleidingen</p>

<div class="manual-container">
	<article>
		<a href="https://eskidoos.be/pdf/handleiding-starten-met-bloggen-wordpress-ESKIDOOS.pdf" target="_blank">
			<div class="academy-item__imageholder"><img src="<?= plugins_url('', dirname(__FILE__)); ?>/assets/images/banner_wordpress.jpg" alt="Bloggen met Wordpress" /></div>
			<span>Starten met bloggen op WordPress</span>
		</a>
	</article>
</div>