<?php

/**
 * @package  Eskidoos
 */

namespace Inc\Base;

use Inc\Base\BaseController;

/**
 * 
 */
class Enqueue extends BaseController
{
	function register()
	{
		add_action('admin_enqueue_scripts', array($this, 'enqueue'));
	}

	function enqueue()
	{

		$pluginVersion = get_plugin_data($this->plugin_path . 'Eskidoos.php');
		$pluginVersion = !empty($pluginVersion) ? $pluginVersion['Version'] : '1.0.0';

		// enqueue all our scripts
		wp_enqueue_style('eskidoosfont', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap');
		wp_enqueue_style('eskidoosstyle', $this->plugin_url . '/assets/css/style.css', [], $pluginVersion);
		wp_enqueue_script('eskidoosscript', $this->plugin_url . '/assets/js/main.js', [], $pluginVersion);
	}
}
