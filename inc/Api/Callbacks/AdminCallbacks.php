<?php

/**
 * @package  Eskidoos
 */

namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

class AdminCallbacks extends BaseController
{
	public function adminDashboard()
	{
		return require_once("$this->plugin_path/templates/admin.php");
	}

	public function adminAcademy()
	{
		return require_once("$this->plugin_path/templates/academy.php");
	}

	public function adminSupport()
	{
		return require_once("$this->plugin_path/templates/adminSupportPage.php");
	}

	public function adminManual()
	{
		return require_once("$this->plugin_path/templates/adminManualPage.php");
	}
}
