<?php

/**
 * @package Eskidoos
 */

namespace Inc;

class Customizer
{

	function __construct()
	{
		self::showLogo();
		self::customLogin();
	}

	function showLogo()
	{
		add_action('admin_bar_menu', array(__CLASS__, 'add_custom_logo'), 10);
	}

	function customLogin()
	{
		add_action('login_enqueue_scripts', array(__CLASS__, 'eskidoos_login_logo'), 11);
		add_filter('login_headerurl', array(__CLASS__, 'eskidoos_login_logo_url'));
		add_filter('login_headertext', array(__CLASS__, 'eskidoos_login_logo_url_title'));
	}

	// Add custom to Admin Bar
	public static function add_custom_logo($meta = TRUE)
	{

		global $wp_admin_bar;
		if (!is_user_logged_in()) {
			return;
		}
		$wp_admin_bar->add_menu(
			array(
				'id' => 'customlogo',
				'title' => '<img src="' . esc_url(plugins_url('../assets/images/large.svg', __FILE__)) . '" alt="Logo Eskidoos"/>',
				'href' => '/wp-admin',
				'meta'  => array('target' => '_self')
			)
		);
	}


	public static function eskidoos_login_logo()
	{ ?>
		<style type="text/css">
			#login h1 a,
			.login h1 a {
				background-image: url(<?php echo esc_url(plugins_url('../assets/images/logo-eskidoos.svg', __FILE__)) ?>);
				width: 250px;
				background-size: contain;
				background-repeat: no-repeat;
				background-position: bottom;
				margin: 30px auto;
			}

			.login #login .message,
			.login #login .success,
			.login #login #login_error {
				border-left-color: #4FBFBD;
			}

			.wp-core-ui #loginform .button-primary {
				background-color: #4FBFBD;
				border-color: #4FBFBD;
			}

			.wp-core-ui #loginform .button-secondary {
				color: #4FBFBD;
			}
		</style>
<?php }

	public static function eskidoos_login_logo_url()
	{
		return "https://eskidoos.be";
	}


	public static function eskidoos_login_logo_url_title()
	{
		return 'Eskidoos.be';
	}
}
