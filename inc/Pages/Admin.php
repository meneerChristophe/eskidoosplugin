<?php

/**
 * @package  Eskidoos
 */

namespace Inc\Pages;

use Inc\Api\SettingsApi;
use Inc\Base\BaseController;
use Inc\Api\Callbacks\AdminCallbacks;

/**
 * 
 */
class Admin extends BaseController
{
	public $settings;

	public $callbacks;

	public $pages = array();

	public $subpages = array();

	public function register()
	{
		$this->settings = new SettingsApi();

		$this->callbacks = new AdminCallbacks();

		$this->setPages();

		$this->setSubpages();

		$this->setSettings();
		$this->setSections();
		$this->setFields();

		$this->settings->addPages($this->pages)->withSubPage('ESKIDOOS')->addSubPages($this->subpages)->register();
	}

	public function setPages()
	{
		$this->pages = array(
			array(
				'page_title' => 'Eskidoos Plugin',
				'menu_title' => 'ESKIDOOS',
				'capability' => 'manage_options',
				'menu_slug' => 'eskidoos_plugin',
				'callback' => array($this->callbacks, 'adminDashboard'),
				'icon_url' => 'dashicons-thumbs-up',
				'position' => 110
			)
		);
	}

	public function setSubpages()
	{
		$this->subpages = array(
			array(
				'parent_slug' => 'eskidoos_plugin',
				'page_title' => 'Academy page',
				'menu_title' => 'Academy',
				'capability' => 'manage_options',
				'menu_slug' => 'eskidoos_academy',
				'callback' => array($this->callbacks, 'adminAcademy')
			),
			array(
				'parent_slug' => 'eskidoos_plugin',
				'page_title' => 'Support page',
				'menu_title' => 'Support',
				'capability' => 'manage_options',
				'menu_slug' => 'eskidoos_support',
				'callback' => array($this->callbacks, 'adminSupport')
			),
			array(
				'parent_slug' => 'eskidoos_plugin',
				'page_title' => 'Handleiding pagina',
				'menu_title' => 'Handleiding',
				'capability' => 'manage_options',
				'menu_slug' => 'eskidoos_manual',
				'callback' => array($this->callbacks, 'adminManual')
			),
		);
	}

	public function setSettings()
	{
		$args = array(
			array(
				'option_group' => 'eskidoos_options_group',
				'option_name' => 'text_example',
				'callback' => array($this->callbacks, 'eskidoosOptionsGroup')
			),
			array(
				'option_group' => 'eskidoos_options_group',
				'option_name' => 'first_name'
			)
		);

		$this->settings->setSettings($args);
	}

	public function setSections()
	{
		$args = array(
			array(
				'id' => 'eskidoos_admin_index',
				'title' => 'Settings',
				'callback' => array($this->callbacks, 'eskidoosAdminSection'),
				'page' => 'eskidoos_plugin'
			)
		);

		$this->settings->setSections($args);
	}

	public function setFields()
	{
		$args = array();

		$this->settings->setFields($args);
	}
}
