Plugin updaten:

- Na uw fantastische changes eerst in Eskidoos.php bovenaan de versie updaten

Nu gaan we enkele git commando's doen

- optioneel: git status in de root van de plugin om te kijken welke files er veranderd zijn.
- git add -A (commando om de aangepaste bestanden in de staging area te plaatsen)
- git commit -m "adding..." (bericht over wat je gedaan hebt)
- git tag -a v1.4 -m "my version 1.4" (versie moet dezelfde zijn als in de Eskidoos.php file, anders blijft hij een melding geven dat de plugin moet upgedate worden)
- git push origin master (staging area live op bitbucket zetten)
- git push --tags (dit is ook nodig aangezien deze niet automatisch worden meegepusht)

Belangrijk als je verder wil werken aan de plugin, doet dan zeker 'git pull origin master' om de laatste changes binnen te trekken op je pc.