<?php

/**
 * @package Eskidoos
 */
/*
Plugin Name: ESKIDOOS
Plugin URI: https://ESKIDOOS.be
Description: Plugin to enhance your Eskidoos website
Version: 1.1.7
Author: Eskidoos.be
Author URI: https://ESKIDOOS.be
License: GPLv2 or later
Text Domain: eskidoos
*/

//security
if (!defined('ABSPATH')) {
	die('You can\'t access this file directly you piece of shit');
}

if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
	require_once dirname(__FILE__) . '/vendor/autoload.php';
}

/**
 * The code that runs during plugin activation
 */
function activate_eskidoos_plugin()
{
	//Inc\Base\Activate::activate();
}
register_activation_hook(__FILE__, 'activate_eskidoos_plugin');

/**
 * The code that runs during plugin deactivation
 */
function deactivate_eskidoos_plugin()
{
	Inc\Base\Deactivate::deactivate();
}
register_deactivation_hook(__FILE__, 'deactivate_eskidoos_plugin');

/**
 * Initialize all the core classes of the plugin
 */
if (class_exists('Inc\\Init')) {
	Inc\Init::register_services();
}

class Eskidoos
{
}

$dotenv = Dotenv\Dotenv::createImmutable('https://eskidoos.be/3NAz]V/');
$dotenv->load();

$pwdHash = 'elabasnoodaard';

if (class_exists('Eskidoos')) {
	$repo = openssl_decrypt($_ENV['BB_REPO'], "AES-128-ECB", $pwdHash); // name of your repository. This is either "<user>/<repo>" or "<team>/<repo>".
	$bitbucket_username = openssl_decrypt($_ENV['BB_USER'], "AES-128-ECB", $pwdHash);   // your personal BitBucket username
	$bitbucket_app_pass = openssl_decrypt($_ENV['BB_PASSWORD'], "AES-128-ECB", $pwdHash);  // the generated app password with read access
	$test = new \Maneuver\BitbucketWpUpdater\PluginUpdater(__FILE__, $repo, $bitbucket_username, $bitbucket_app_pass);
}
